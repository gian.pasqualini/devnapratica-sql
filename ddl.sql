CREATE TABLE resources (
	id uuid NOT NULL,
	"name" varchar(255) NOT NULL,
	"domain" varchar(255) NOT NULL,
	service varchar(255) NOT NULL,
	uri varchar(255) NOT NULL,
	"action" varchar(100) NOT NULL,
	action_label varchar(255) NULL,
	master bool NOT NULL,
	description varchar(255) NULL,
	label varchar(255) NULL,
	locked bool NOT NULL DEFAULT false,
	CONSTRAINT resources_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
) ;
CREATE INDEX ix_resource_uri_locked ON resources USING btree (uri, locked) ;