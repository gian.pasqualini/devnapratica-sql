CREATE TABLE resource_audits (
   id SERIAL PRIMARY KEY,
   uri varchar(255) NOT NULL,
   changed_on TIMESTAMP(6) NOT NULL
);

CREATE OR REPLACE FUNCTION log_last_inserts()
  RETURNS trigger AS
$BODY$
BEGIN   
    INSERT INTO resource_audits(uri,changed_on)
    VALUES(NEW.uri,now());

	RETURN NEW;
END;
$BODY$

LANGUAGE plpgsql VOLATILE 
COST 100; 

--**************************************************

CREATE TRIGGER last_inserts
  BEFORE INSERT
  ON resources
  FOR EACH ROW
  EXECUTE PROCEDURE log_last_inserts();
  
--**************************************************

INSERT INTO  resources (id,"name","domain",service,uri,"action",action_label,master,description,label,locked) VALUES 
('ec9571e5-a390-435e-9b3d-a03d65a00000','Meu Teste','Dev Na Pratica','menu','res://senior.com.br/custom/menu/1475810d-72e5-4a55-af44-8e4f86a3xxxx','Visualizar',NULL,false,NULL,'Site Senior',false);